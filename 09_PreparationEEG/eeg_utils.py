import mne
import pandas as pd
import numpy as np

# how to use:
"""
1) filePreparation
2) annotate blink events
3) get epochs (those without blinks)

"""


def filePreparation(nameOfSubect="ACE11B1"):
    
    raw =  mne.io.read_raw_brainvision(
        "../00_Data/00_RelevanteDaten/EEG/IE_SRT_"+nameOfSubect+".vhdr",
        
        #designated EOG Channels
        eog = ["heog_li","heog_re","veog"],
        preload=True
    )
    
    
    #1. cut signal to a time span that covers the first 240 trials
    events = mne.find_events(raw)
    # 5 sekunden vor erstem stimulus
    begin_time = events[0,0]/raw.info["sfreq"] -5
    # begin_time = 0
    # 5 sekunden nach 484. Stimulus (erster Random-Block sollte 4*2*60 stimuli/reaktionen + 4 starts enthalten, =484 )
    end_time =  events[483,0]/raw.info["sfreq"] + 5

    raw.crop(begin_time, end_time)
    
    
    #2. rename LM and RM
    raw.rename_channels(mapping={"LM":"TP9"})
    raw.rename_channels(mapping={"RM" :"TP10"})
    
    
    #3. add locations of electrodes
    montage = mne.channels.read_montage('standard_1020')
    raw.set_montage(montage)
    
    #4. info about subject - gelöscht, verursaht probleme beim schreiben in datei
    #raw.info["subject_info"] = {"id":nameOfSubect }
    
    return raw
    
def annotateBlinks(raw):
    """
    As the data is epoched, all the epochs overlapping with segments whose description starts with ‘bad’ are rejected by default. To turn rejection off, use keyword argument reject_by_annotation=False when constructing mne.Epochs. When working with neuromag data, the first_samp offset of raw acquisition is also taken into account the same way as with event lists. For more see mne.Epochs and mne.Annotations.
    
    """
    
    
    eog_events = (mne.preprocessing.find_eog_events(raw, ch_name="veog"))
    n_blinks = len(eog_events)
    onset = eog_events[:, 0] / raw.info['sfreq'] -0.25
    duration = np.repeat(0.5, n_blinks)

    annot= mne.Annotations(onset, duration, ['bad blink'] * n_blinks,  orig_time=raw.info['meas_date'])

    raw =raw.set_annotations(annot)
    return raw
    

def getStimulusEpochs(raw, tmin=-0.4, tmax=0.8,baseline=(-0.1,0), rejectEpochsWithBlinks=True, proj=False,
                      
                      rejectPeakToPeak =None
                     ):
    
    """
    only evnts and epochs around stimuli
    returns:
    -  events, epochs

    attributes
    - raw: raw eeg file
    - tmin  # start of each epoch (e.g. 200ms before the trigger)
    - tmax =  # end of each epoch (e.g 500ms after the trigger)
    - baseline=(-0.1,0): to epoch-wise normalise the absolute values of the signal (mean of 100 ms befor until the stimulus is taken)
    - rejectEpochsWithBlinks: True, then blinks are dropped
    - rejectPeakToPeak: dict: see Epochs Constructor
    
    """
    #only stimuli are taken
    event_id = dict(
                stim_1=21, stim_2=22,stim_3=23,stim_4=24
               )
    
    events = mne.find_events(raw)
    
    epochs = mne.Epochs(raw,events, 
                        event_id=event_id,
                        tmin=tmin,
                        tmax=tmax, 
                        proj=proj,
                        baseline=(-0.1,0),
                        
                        reject=rejectPeakToPeak,
                        #Epochs overlapping with segments whose description begins with bad (bad blink)
                        reject_by_annotation = rejectEpochsWithBlinks
    
                       )
    
    epochs.drop_bad()
    

    return events, epochs

